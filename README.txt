INTRODUCTION
------------

This light-weight module extends the Drupal Commerce module by providing the
ability to configure the discount amount, product and timeframe to run discount
campaigns.

HOW IT WORKS
------------

Commerce Discount Campaign settings page has several configuration options:

1) Name for the discount campaign, which will be used as a price component title
in the order invoice;
2) Timeframe during which discount campaign is effective;
3) Discount type to choose between amount or percentage;
4) Discount amount above zero;
5) Product SKU or SKU pattern.

The discount value will be calculated based on the set amount and type, and is
applied towards new orders with the matched SKU that are placed during the set
campaign period.

ALTERNATIVE MODULES
-------------------

In comparison with Commerce Discount (http://dgo.to/commerce_discount) module,
the Commerce Discount Campaign is much lighter solution.

The Commerce Discount is not just a module, but rather a complex ecosystem which
relies on Rules and can be accompanied by number of other modules as Commerce
Coupon, Commerce Discount Extra, Commerce Discount Product Category, Commerce
Discount Taxonomy Conditions. The Commerce Discount is dependent on such modules
as Inline Entity Form, Inline Conditions, Entity Reference and comes with lot's
of features such as to run as the project page says:

> promotions through the use of a custom entity type, including fields to
> configure how a discount is displayed in the cart, usage counts, valid date
> ranges

So go with it if your commerce setup needs features provided by the Commerce
Discount module. However, if you are looking for something simple just to run
temporary campaigns on your Drupal website, then the Commerce Discount Campaign
module is your best choice.

INSTALLATION
------------

* Download the module to your website's modules directory. You can use
  `drush dl discount_campaign` for that.
* Go to the Modules page (/admin/modules) and enable the module. Alternatively,
  just run `drush en discount_campaign` command on CLI.
* Go to the Commerce Discount Campaign settings page
  (admin/commerce/config/discount_campaign), set desired configuration options.
* That's it! Next time someone places an order on your website, they will get
  the calculated discount.
